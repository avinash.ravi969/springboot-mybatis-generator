package com.example.mybatis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * 
 * @author Avinash
 *
 */
@Configuration
@ImportResource({"classpath:/config/spring-mybatis.xml"})
public class MybatisConfig {

}
