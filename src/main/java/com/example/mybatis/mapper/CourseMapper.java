package com.example.mybatis.mapper;

import com.example.mybatis.domain.Course;
import com.example.mybatis.domain.CourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface CourseMapper {
    long countByExample(CourseExample example);

	int deleteByExample(CourseExample example);

	int deleteByPrimaryKey(Integer courseId);

	int insert(Course record);

	int insertSelective(Course record);

	List<Course> selectByExampleWithRowbounds(CourseExample example, RowBounds rowBounds);

	List<Course> selectByExample(CourseExample example);

	Course selectByPrimaryKey(Integer courseId);

	int updateByExampleSelective(@Param("record") Course record, @Param("example") CourseExample example);

	int updateByExample(@Param("record") Course record, @Param("example") CourseExample example);

	int updateByPrimaryKeySelective(Course record);

	int updateByPrimaryKey(Course record);
}