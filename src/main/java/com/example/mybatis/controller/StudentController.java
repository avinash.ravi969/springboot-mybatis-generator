/**
 * 
 */
package com.example.mybatis.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.mybatis.domain.Student;
import com.example.mybatis.domain.StudentExample;
import com.example.mybatis.dto.StudentDto;
import com.example.mybatis.mapper.StudentMapper;

/**
 * @author Avinash
 *
 */
@RestController
@RequestMapping("/api")
public class StudentController {
	
	@Autowired
	StudentMapper studentMapper;

	@RequestMapping(value="/student", method= RequestMethod.POST)
	public ResponseEntity<StudentDto> createStudent(@RequestBody Student student) {
		studentMapper.insert(student);
		StudentDto studentDto = new StudentDto();
		studentDto.setEmail(student.getEmail());
		return new ResponseEntity<StudentDto>(studentDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/student", method= RequestMethod.GET)
	public ResponseEntity<List<StudentDto>> getAllStudents() {
		
		StudentExample example = new StudentExample();
		example.or().andStudentIdLessThan(2);
		
		List<Student> results = studentMapper.selectByExample(example);
		List<StudentDto> students = new ArrayList<>();
		
		for (Student result : results) {
			StudentDto studentDto = new StudentDto();
			studentDto.setEmail(result.getEmail());
			studentDto.setName(result.getStudentName());
			studentDto.setPhoneNum(result.getPhonenum());
			students.add(studentDto);
		}
		return new ResponseEntity<List<StudentDto>>(students, HttpStatus.OK);
	}

}
